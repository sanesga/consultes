/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.util.List;
import java.util.Scanner;
import model.Profesor;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

/**
 *
 * @author Maite
 */
public class App {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner teclado = new Scanner(System.in);

        int opcion;
        Profesor p = null;
        //CREAMOS CONEXION
        SessionFactory sessionFactory;
        Configuration configuration = new Configuration();
        configuration.configure();
        sessionFactory = configuration.buildSessionFactory();
        SessionFactory factory = new Configuration().configure().buildSessionFactory();
        Query query;

        //CREAR UNA SESION
        Session session = factory.openSession();

        do {
            System.out.println("Elige opción");
            menu();
            opcion = teclado.nextInt();

            switch (opcion) {
                case 1://CREAR SEGURO
                    p = new Profesor(7, "Pepe", "Garcia", "Perez");
                    System.out.println("Profesor creado con éxito");
                    break;
                case 2://GUARDAR SEGURO
                    session.beginTransaction();
                    session.save(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor guardado con éxito");
                    break;
                case 3://LEER SEGURO
                    p = session.get(Profesor.class, 1);
                    System.out.println(p);
                    break;
                case 4://ACTUALIZAR
                    session.beginTransaction();
                    p.setApe1("Rodríguez");
                    session.update(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor actualizado con éxito");
                    break;
                case 5://ELIMINAR SEGURO
                    session.beginTransaction();
                    session.delete(p);
                    session.getTransaction().commit();
                    System.out.println("Profesor eliminado con éxito");
                    break;
                case 6:
                    //Profesor es el nom de la classe!!!!!
                    query = session.createQuery("SELECT p FROM Profesor p");
                    List<Profesor> profesores = query.list();
                    for (Profesor profesor : profesores) {
                        System.out.println(profesor);
                    }

                    query = session.createQuery("SELECT p.id, p.nombre FROM Profesor p");
                    List<Object[]> listaDatos = query.list();
                    for (Object[] datos : listaDatos) {
                        System.out.println(datos[0] + "--" + datos[1]);
                    }

                    query = session.createQuery("SELECT p.nombre FROM Profesor p");
                    List<Object> listaNombre = query.list();
                    for (Object nombre : listaNombre) {
                        System.out.println(nombre);
                    }
                    //UNIQUE RESULT
                    Profesor profesor = (Profesor) session.createQuery("SELECT p FROM Profesor p WHERE id=1001").uniqueResult();
                    System.out.println(profesor);

                    //PAGINACIÓN (MOSTRAR LA PAGINA QUE QUERAMOS)
                    int tamanyoPagina = 10;
                    int paginaAMostrar = 7;
                    query = session.createQuery("SELECT p FROM Profesor p Order By p.id");
                    query.setMaxResults(tamanyoPagina);
                    query.setFirstResult(paginaAMostrar * tamanyoPagina);
                    List<Profesor> profes = query.list();
                    for (Profesor p1 : profes) {
                        System.out.println(p1);
                    }

                    //CONSULTAS POR NOMBRE
                    query = session.getNamedQuery("findAllProfesores");
                    profesores = query.list();
                    for (Profesor p2 : profesores) {
                        System.out.println(p2);
                    }
                    
                    //CONSULTA CON PARÁMETROS POR NOMBRE
                    String nombre="ISIDRO";
                    String ape1="CORTINA";
                    String ape2="GARCÍA";
                    query=session.createQuery("SELECT p FROM Profesor p WHERE nombre=:nombre AND ape1=:ape1 AND ape2=:ape2");
                    query.setString("nombre", nombre);
                    query.setString("ape1", ape1);
                    query.setString("ape2", ape2);
                    profesores = query.list();
                    for (Profesor p3 : profesores){
                        System.out.println(p3);
                    }
                    
                    break;
                case 7://SALIR
                    //cerrar la conexion
                    session.close();
                    sessionFactory.close();
                    System.out.println("Saliendo..");
                    break;
                default:
                    throw new AssertionError();
            }
        } while (opcion != 7);

    }

    public static void menu() {
        System.out.println("CONSIDERACIÓN 1. CREAR SIEMPRE LOS OBJETOS ANTES DE GUARDARLOS");
        System.out.println("CONSIDERACIÓN 2. RECORDAR SALIR DE LA APLICACIÓN PARA CERRAR SESIÓN");
        System.out.println("CONSIDERACIÓN 3. SI SE INTENTA VOLVER A AÑADIR PROFESOR, COMO SON FIJOS DARÁ ERROR");
        System.out.println("1. Crear profesor");
        System.out.println("2. Guardar profesor");
        System.out.println("3. Leer profesor");
        System.out.println("4. Actualizar profesor");
        System.out.println("5. Eliminar profesor");
        System.out.println("6. Realizar una consulta");
        System.out.println("7. Salir");
    }
}
